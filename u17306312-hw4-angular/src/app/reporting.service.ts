import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { fromEvent } from "rxjs";
import { map, filter, debounce, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportingService {

  constructor(
    private http: HttpClient
  ) { }

  getReportingData(selection) {
    return this.http.get(``).pipe(map(res => res));
  }

  downloadReport(selection, type) {
    return this.http.get(``, { responseType: 'blob' });
  }
}
