import { Component } from '@angular/core';

import { HttpClient } from "@angular/common/http";
import { Chart } from "charts.js";
import { ReportingService } from "./reporting.service";
import { mergeMap, groupBy, map, reduce } from "rxjs/operators";
import { of } from "rxjs";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent {
	title = 'u17306312-hw4-angular';

	chart = [];
	courses: Object = null;
	options = [
		{ id: 1, text: "text for one" },
		{ id: 2, text: "text for two" },
		{ id: 3, text: "text for three" }
	];
	selection: Number = 3;

	constructor(
		private reporting: ReportingService
	) {}

	submitRequest() {
		this.reporting.getReportingData(this.selection).subscribe(res => {
			console.log(res);

			let keys = res[""].map(d->d.Name);
			let values = res[""].map(d->d.Average);

			this.courses = res["courses"];

			this.chart = new Chart('canvas', {
				type: 'bar',
				data: {
					labels: keys,
					datasets: [
						{
							data: values,
							borderColor: '#3cba9f',
							fill: false,
							backgroundColor: '#fff'
						}
					]
				},
				options: {
					legend: {
						display: false
					},
					title: {
						display: true,
						text: "NorthWind Report"
					},
					scales: {
						xAxes: [{
							display: true,
							barPercentage: 0.75
						}],
						yAxes: [{
							display: true,
							ticks: {
								min: 0,
								max: 100
							}
						}]
					}
				}
			})
		})
	}

	downloadReport(type) {
		this.reporting.downloadReport(this.selection, type).subscribe(x => {
			
		})
	}

}
