﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using u17306312_HW4.Models;

namespace u17306312_HW4.Controllers
{
    public class CustomersController : ApiController
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: api/Customers
        public List<dynamic> GetCustomers()
        {
            //return db.Customers.ToList();

            List<dynamic> dynamicType = new List<dynamic>();

            foreach (Customer c in db.Customers.ToList()) {
                dynamic rl = new ExpandoObject();

                rl.CustomerID = c.CustomerID;
                rl.CompanyName = c.CompanyName;
                rl.ContactName = c.ContactName;
                rl.ContactTitle = c.ContactTitle;
                rl.Address = c.Address;
                rl.City = c.City;
                rl.Region = c.Region;
                rl.PostalCode = c.PostalCode;
                rl.Country = c.Country;
                rl.Phone = c.Phone;
                rl.Fax = c.Fax;

                dynamicType.Add(rl);
            }

            return dynamicType;
        }

        // GET: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult GetCustomer(string id) {
            Customer customer = db.Customers.Find(id);
            if (customer == null) {
                return NotFound();
            }

            return Ok(customer);
        }
        
        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer(string id, Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customer.CustomerID)
            {
                return BadRequest();
            }

            db.Entry(customer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Customers.Add(customer);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CustomerExists(customer.CustomerID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = customer.CustomerID }, customer);
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(Customer))]
        public IHttpActionResult DeleteCustomer(string id)
        {
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return NotFound();
            }

            db.Customers.Remove(customer);
            db.SaveChanges();

            return Ok(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CustomerExists(string id)
        {
            return db.Customers.Count(e => e.CustomerID == id) > 0;
        }
    }
}