﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using u17306312_HW4.Models;


namespace u17306312_HW4.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportsController : ApiController
    {
        // GET: api/Reports/getReportData
        [System.Web.Mvc.Route("Reports/getReportData")]
        [HttpGet]
        public dynamic Get(int selection)
        {
            NorthwindEntities db = new NorthwindEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<Order> orders = null;

            if (selection == 1) {

            } else if (selection == 2) {

            } else {    // get all orders
                orders = db.Orders.ToList();
            }

            return getExpandedReport(orders);
            //return null;
        }


        private dynamic getExpandedReport(List<Order> orders) {
            if (orders == null)
                return null;
            else {
                NorthwindEntities db = new NorthwindEntities();
                db.Configuration.ProxyCreationEnabled = false;

                dynamic outObj = new ExpandoObject();
                var list = orders.ToList();

                List<dynamic> dynamicList = new List<dynamic>();
                foreach (var g in list) {
                    dynamic d = new ExpandoObject();
                    var order = db.Order_Details.Where(zz => zz.OrderID == g.OrderID).FirstOrDefault();
                    var customer = db.Customers.Where(zz => zz.CustomerID == g.CustomerID).FirstOrDefault();
                    //var employee = db.Employees.Where(zz => zz.EmployeeID == g.EmployeeID).FirstOrDefault();

                    d.Order_Details = order;
                    d.Customer = customer;
                    //d.Employee = employee;

                    d.OrderDate = g.OrderDate;
                    d.RequiredDate = g.RequiredDate;
                    d.ShippedDate = g.ShippedDate;
                    d.ShipVia = g.ShipVia;
                    d.Freight = g.Freight;
                    d.ShipName = g.ShipName;
                    d.ShipAddress = g.ShipAddress;
                    d.ShipCity = g.ShipCity;
                    d.ShipRegion = g.ShipRegion;
                    d.ShipPostalCode = g.ShipPostalCode;
                    d.ShipCountry = g.ShipCountry;
                    d.Customer = g.Customer;
                    d.Employee = g.Employee;
                    d.Shipper = g.Shipper;

                    dynamicList.Add(d);
                }

                return dynamicList;
                //return list;
            }
        }

        //// GET: api/Reports/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Reports
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Reports/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Reports/5
        //public void Delete(int id)
        //{
        //}
    }
}
