﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using u17306312_HW4.Models;

namespace u17306312_HW4.Controllers
{
    public class ProductsController : ApiController
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: api/Products
        public List<dynamic> GetProducts()
        {
            //return db.Products;

            List<dynamic> d = new List<dynamic>();

            foreach (Product od in db.Products.ToList()) {
                dynamic gg = new ExpandoObject();

                gg.ProductID = od.ProductID;
                gg.ProductName = od.ProductName;
                gg.SupplierID = od.SupplierID;
                gg.CategoryID = od.CategoryID;
                gg.QuantityPerUnit = od.QuantityPerUnit;
                gg.UnitPrice = od.UnitPrice;
                gg.UnitsInStock = od.UnitsInStock;
                gg.UnitsOnOrder = od.UnitsOnOrder;
                gg.ReorderLevel = od.ReorderLevel;
                gg.Discontinued = od.Discontinued;

                d.Add(gg);
            }

            return d;
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductID)
            {
                return BadRequest();
            }

            db.Entry(product).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(product);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = product.ProductID }, product);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.ProductID == id) > 0;
        }
    }
}