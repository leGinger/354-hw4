﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using u17306312_HW4.Models;

namespace u17306312_HW4.Controllers
{
    public class EmployeesController : ApiController
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: api/Employees
        public List<dynamic> GetEmployees()
        {
            //return db.Employees;

            List<dynamic> dynamicType = new List<dynamic>();

            foreach (Employee c in db.Employees.ToList()) {
                dynamic rl = new ExpandoObject();

                rl.EmployeeID = c.EmployeeID;
                rl.LastName = c.LastName;
                rl.FirstName = c.FirstName;
                rl.Title = c.Title;
                rl.TitleOfCourtesy = c.TitleOfCourtesy;
                rl.BirthDate = c.BirthDate;
                rl.HireDate = c.HireDate;
                rl.Address = c.Address;
                rl.City = c.City;
                rl.Region = c.Region;
                rl.PostalCode = c.PostalCode;
                rl.Country = c.Country;
                rl.HomePhone = c.HomePhone;
                rl.Extension = c.Extension;
                rl.Photo = c.Photo;
                rl.Notes = c.Notes;
                rl.ReportsTo = c.ReportsTo;
                rl.PhotoPath = c.PhotoPath;

                dynamicType.Add(rl);
            }

            return dynamicType;
        }

        // GET: api/Employees/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        // PUT: api/Employees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee(int id, Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employee.EmployeeID)
            {
                return BadRequest();
            }

            db.Entry(employee).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employees
        [ResponseType(typeof(Employee))]
        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee.EmployeeID }, employee);
        }

        // DELETE: api/Employees/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult DeleteEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employee);
            db.SaveChanges();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeID == id) > 0;
        }
    }
}