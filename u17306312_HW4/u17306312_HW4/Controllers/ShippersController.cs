﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using u17306312_HW4.Models;

namespace u17306312_HW4.Controllers
{
    public class ShippersController : ApiController
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: api/Shippers
        public List<dynamic> GetShippers()
        {
            //return db.Shippers;

            List<dynamic> d = new List<dynamic>();

            foreach (Shipper od in db.Shippers.ToList()) {
                dynamic gg = new ExpandoObject();

                gg.ShipperID = od.ShipperID;
                gg.CompanyName = od.CompanyName;
                gg.Phone = od.Phone;
                
                d.Add(gg);
            }

            return d;
        }

        // GET: api/Shippers/5
        [ResponseType(typeof(Shipper))]
        public IHttpActionResult GetShipper(int id)
        {
            Shipper shipper = db.Shippers.Find(id);
            if (shipper == null)
            {
                return NotFound();
            }

            return Ok(shipper);
        }

        // PUT: api/Shippers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShipper(int id, Shipper shipper)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shipper.ShipperID)
            {
                return BadRequest();
            }

            db.Entry(shipper).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShipperExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Shippers
        [ResponseType(typeof(Shipper))]
        public IHttpActionResult PostShipper(Shipper shipper)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Shippers.Add(shipper);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shipper.ShipperID }, shipper);
        }

        // DELETE: api/Shippers/5
        [ResponseType(typeof(Shipper))]
        public IHttpActionResult DeleteShipper(int id)
        {
            Shipper shipper = db.Shippers.Find(id);
            if (shipper == null)
            {
                return NotFound();
            }

            db.Shippers.Remove(shipper);
            db.SaveChanges();

            return Ok(shipper);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShipperExists(int id)
        {
            return db.Shippers.Count(e => e.ShipperID == id) > 0;
        }
    }
}